<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/context.php';

use Interop\Amqp\AmqpQueue;
use Interop\Amqp\AmqpMessage as InteropAmqpMessage;

class Consumer
{
    private $queueName;
    private $context;
    public function __construct($queueName)
    {
        $this->queueName = $queueName;
        $this->context = Context::getConnection();

    }

    public function getMessage()
    {

        $queue = $this->context->createQueue($this->queueName);

        $consumer = $this->context->createConsumer($queue);

        $message = $consumer->receive();

        // on notifie qu'on a reçu, on peut aussi reject
        $consumer->acknowledge($message);

        return $message;
    }
}
