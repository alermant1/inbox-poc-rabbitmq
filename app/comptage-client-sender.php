<?php

require_once __DIR__ . '/sender.php';
require_once __DIR__ . '/consumer.php';

// on fait la demande du comptage
$sender = new Sender('inbox.target', 'comptage', uniqid(), "Ceci est mon comptage");
// on créé une queue temporaire (la réponse)
$sender->withQueryResponse(true);
$queueResponse = $sender->send();

$consumer = new Consumer($queueResponse);

// on va récupérer la réponse !
$reponse = $consumer->getMessage();

print_r($reponse);

// on supprime la queue temporaire
$sender->cleanTemporaryQueue();
