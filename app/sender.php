<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/context.php';

use Interop\Amqp\AmqpTopic;
use Interop\Amqp\AmqpQueue;
use Interop\Amqp\Impl\AmqpBind;
use Interop\Amqp\AmqpQueue as InteropAmqpQueue;

class Sender
{
    // ex: 'inbox.target'
    private $topicName;
    // ex: 'comptage
    private $queueName;
    // ex: '{"query": "ma requete"}'
    private $queryBody;
    // ex : 1337
    private $uniqueId;
    // context qui permet la connection
    private $context;
    // si on souhaite avoir une réponse, on créer une queue
    private $withQueryResponse;
    private $temporaryQueue;

    public function __construct(
        $topicName,
        $queueName,
        $uniqueId,
        $queryBody = ''
    ) {
        $this->topicName = $topicName;
        $this->queueName = $queueName;
        $this->uniqueId = $uniqueId;
        $this->queryBody = $queryBody;
        $this->context = Context::getConnection();
        $this->withQueryResponse = false;
        $this->temporaryQueue = null;
    }

    public function withQueryResponse($withQueryResponse)
    {
        $this->withQueryResponse = $withQueryResponse;
    }

    /**
     * Envoie le message et retourne la file d'attente
     * qui devra etre utilisé pour écouter la réponse
     */
    public function send()
    {

        $topic = $this->context->createTopic($this->topicName."2");
        $topic->setType(AmqpTopic::TYPE_FANOUT);
        $this->context->declareTopic($topic);


        $queue = $this->context->createQueue($this->queueName);
        $queue->addFlag(AmqpQueue::FLAG_DURABLE);
        $this->context->declareQueue($queue);
        $this->context->bind(new AmqpBind($topic, $queue));

        $queueResponse = "{$this->queueName}.{$this->uniqueId}";

        $message = $this->context->createMessage(
            $this->queryBody,
            [
                'created_at' => (new \DateTime)->format('Y-m-d H:i:s'),
                'queueResponse' => $queueResponse
            ],
            ['message_id' => $this->uniqueId]
        );

        $this->context->createProducer()->send($queue, $message);

        if ($this->withQueryResponse) {
            $this->temporaryQueue = $this->context->createQueue($queueResponse);
            $this->temporaryQueue->addFlag(AmqpQueue::FLAG_DURABLE);
            $this->context->declareQueue($this->temporaryQueue);
            $this->context->bind(new AmqpBind($topic, $this->temporaryQueue));
        }

        return $queueResponse;
    }

    /**
     * Vide la queue temporaire
     */
    public function cleanTemporaryQueue()
    {
        if ($this->temporaryQueue instanceof InteropAmqpQueue) {
            $this->context->deleteQueue($this->temporaryQueue);
        }
    }
}
