<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Enqueue\AmqpExt\AmqpConnectionFactory;
use Enqueue\AmqpExt\AmqpContext;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = (new Dotenv)->load(__DIR__.'/../.env');

class Context {

    /**
     * @var Context
     */
    private static $oInstance;

    /**
     * @var Enqueue\AmqpExt\AmqpContext
     */
    private $oAmqpContext;

    private function __construct()
    {
        $this->oAmqpContext = $this->init();
    }

    public static function getConnection()
    {
        if (is_null(self::$oInstance)) {
            self::$oInstance = new Context;
        }

        return self::$oInstance->oAmqpContext;
    }

    public function init()
    {
        // connects to localhost
        $connectionFactory = new AmqpConnectionFactory();

        // same as above
        $factory = new AmqpConnectionFactory('amqp:');

        // same as above
        $factory = new AmqpConnectionFactory([]);

        // connect to AMQP broker
        $factory = new AmqpConnectionFactory([
            'host' => getenv('RABBITMQ_HOST'),
            'port' => getenv('RABBITMQ_PORT'),
            'vhost' => '/',
            'user' => getenv('RABBITMQ_USER'),
            'pass' => getenv('RABBITMQ_PASS'),
            'persisted' => false,
        ]);

        return $factory->createContext();
    }
}
