<?php

require_once __DIR__ . '/consumer.php';
require_once __DIR__ . '/sender.php';

// on écoute les messages
do {
    $consumer = new Consumer('comptage');
    $message = $consumer->getMessage();

    print_r($message);

    // on calcule le résultat
    $reponseBody = "Résultat => " . rand(1337, 13370000);

    $queueResponse = $message->getProperties()['queueResponse'];

    $sender = new Sender('inbox.target', $queueResponse, uniqid(), $reponseBody);
    $sender->send();

} while(1);
