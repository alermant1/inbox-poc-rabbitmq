# Commandes

## Client / consumer

```bash
php app/comptage-client-sender.php
```

## Consumer

```bash
php app/comptage-server-consumer.php
```

# Doc

* https://github.com/php-enqueue/enqueue-dev
* https://www.rabbitmq.com/tutorials/amqp-concepts.html
